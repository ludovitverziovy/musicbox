#!/usr/bin/env python3
import sys
import time
import socket
import argparse
import threading

from .mpv import MPV


class MPVRemote(MPV):
    def _start_thread(self):
        self._thread = threading.Thread(target=self._reader)
        self._thread.setDaemon(True)
        self._thread.start()

    def _start_process(self):
        pass

    def _stop_process(self):
        pass

    def _prepare_socket(self):
        pass

    def _start_socket(self):
        self._sock = socket.socket(socket.AF_UNIX)
        self._sock.connect(self._sock_filename)

    def is_running(self):
        return self._is_running

    def __init__(self, socket_path, debug=False):
        self._sock_filename = socket_path
        self._is_running = True
        super().__init__(debug=debug)

    def close(self):
        self._is_running = False
        super().close()

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        self.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-S', '--socket', default='/tmp/mpvsocket')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-g', '--get', action='store_true')
    group.add_argument('-s', '--set', action='store_true')
    parser.add_argument('args', nargs='+')
    args = parser.parse_args()
    
    with MPVRemote(args.socket) as mpv:
        if args.get:
            print(mpv.get_property(args.args[0]))
        elif args.set:
            mpv.set_property(args.args[0], args.args[1])
        else:
            print(mpv.command(*args.args))

