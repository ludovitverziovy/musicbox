#!/usr/bin/env python
from setuptools import setup

from codecs import open # To use a consistent encoding
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the relevant file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

# get version number
defs = {}
with open(path.join(here, 'musicbox/defs.py')) as f:
    exec(f.read(), defs)

setup(
    name='musicbox',
    version=defs['__version__'],
    description=defs['app_description'],
    long_description=long_description,
    url='https://bitbucket.org/beli-sk/zimbra-auth',
    author="Michal Belica",
    author_email="devel@beli.sk",
    license="AGPL-3",
    include_package_data=True,
    zip_safe=False,
    install_requires=['Flask', 'redis']
    )
